# FCC-Python-projects

A collection of FreeCodeCamp Python Projects
--------------------------------------------

- [x] Arithmetic Formatter - **Complete**
- [ ] Time Calculator - **Incomplete**
- [ ] Budget App - **Incomplete**
- [ ] Polygon Area Calculator - **Incomplete**
- [ ] Probability Calculator - **Incomplete**
