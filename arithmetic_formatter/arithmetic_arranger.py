"""
Turn an array of mathmatical functions into formatted functions.

    Functions:

        len_check(array) -> Exception / Error
        create_list(array) -> list
        numerical_check(list) -> Exceptions / Errors
        calculate(list) -> new list
        formatted_funcs(list, list) -> string
        arithmetic_arranger(array, bool)

    Misc.:

    arithmetic_arranger can take a bool value to alter the outcome of this module.
        bool = True -> Will return a string with formatted arithmetic function and answers.
        bool = False -> Will return a string with formatted arithmetic with no answers.
"""
import operator


def len_check(prob_array):
    """
    Check the array's length is below 5

    If more than 5 items are in the provided array an exception will be raised

    Parameters:
        prob_array (list): a list containing simple math functions

    Returns:
        Print (str): returns an error message to be printed
    """
    if len(prob_array) > 5:
        return 1


def create_list(problem_arr):
    """
    Create a list out of the idividual items within a single array item.

    Parameters:
        problem_arr (list): a list of
    Example:
        arr = [1 + 2, 3 + 4]
        create_list(arr)
        output = [['1', '+' , '2'], ['3', '+', '4']]

    Returns:
        func_list (list): List of individual items provided within array
    """
    func_list = list()
    for problem in problem_arr:
        func_list.append(problem.split(' '))
    return func_list


def numerical_check(function_list):
    """
    A function to do majority of this modules error checking

    Parameters:
        function_list (list): A list of simple mathmatical functions
    Errors:
        The function will provide messages to the following errors
            * Invalid data types
            * Too large of Ints
            * Invalid/unsupported operators

    Returns:
        Str: Error message
        None: Only returned when no errors are encountered
    """
    for function in function_list:
        # if provided values are not int return error
        try:
            isinstance(int(function[0]), int) and  isinstance(int(function[2]), int)
        except ValueError:
            return 1

        # if provided int length > 4 return error
        if len(function[0]) > 4 or len(function[2]) > 4:
            return 2

        # check if supported operator was provided
        if function[1] == '+' or str(function[1]) == '-':
            pass
        else:
            return 3


def calculate(function_list):
    """
    Evaluates the functions that were provided

    Parameters:
    function_list : list
        list to be evaluated

    Returns:
        ans_list (list): contains list of evaluated values
    """
    func_val = lambda val1, operator, val2:  operator(int(val1), int(val2))
    ans_list = list()
    for function in function_list:
        if function[1] == '+':
            ans = func_val(function[0], operator.add, function[2])
            ans_list.append(ans)
        elif function[1] == '-':
            ans = func_val(function[0], operator.sub, function[2])
            ans_list.append(ans)
    return ans_list


def formatted_funcs(function_list, ans, show_ans=False):
    """
    Formats the provided list of functions into a vertical layout

    Parameters:
        function_list (list): list of mathmatical functions to be formatted
        ans (list): list of the provided functions answers

    Returns:
        f-string (str): returns a formatted string to be used in another function
    """
    top = ''
    bottom = ''
    dash = ''
    outcome = ''
    i = 0

    for function in function_list:
        longest = max(len(function[0]), len(function[2]))
        top += f"{function[0]}".rjust(longest+2) + '    '
        bottom += f"{function[1]}" + f"{function[2]}".rjust(longest+1) + '    '
        dash += '-' * (longest+2) + '    '
        outcome += f'{ans[i]}'.rjust(longest+2) + '    '
        i += 1
    # strip extra 4 spaces on end of lines
    top = top.rstrip()
    bottom = bottom.rstrip()
    dash = dash.rstrip()
    outcome = outcome.rstrip()

    if show_ans is True:
        return f'{top}\n{bottom}\n{dash}\n{outcome}'
    return f'{top}\n{bottom}\n{dash}'

def arithmetic_arranger(problems, show=False):
    """
    used in the fashion of a main function  calling other
    sub-functions and processing the provided list of functions

    Parameters:
        problems (list): list of mathmatical functions to be formatted
        show (bool): provide true or false to show or hide formatted functions answers

    Returns:
        output (str): returns a formatted string displaying
        the provided input with a vertical style.
    """
    # Check if array exceeds 5 items
    if len_check(problems) == 1:
        return "Error: Too many problems."
    # seperates function componenets into an iterable state
    val_list = create_list(problems)
    # checks the validity of processed list
    if numerical_check(val_list) == 1:
        return 'Error: Numbers must only contain digits.'
    elif numerical_check(val_list) == 2:
        return 'Error: Numbers cannot be more than four digits.'
    elif numerical_check(val_list) == 3:
        return "Error: Operator must be '+' or '-'."
    else:
        pass
    # perform the calculations and return a list of values
    answers = calculate(val_list)
    output = formatted_funcs(val_list, answers, show)
    return output
