"""
Take a specified 12-hour clock time add a duration and return the time in the 12-hour clock format.

Functions:
    add_time(str, str) -> str

"""


def add_time(start, duration, *dow):
    """
    add duration to start time and return result

    Parameters:
        start (str): string with the value of a 12-hour clock time
        duration (str):  str represents a duration in hours:minutes to be added to start
        *dow (str): optionally add a day of the week

    Returns:
        new_time (str): sum of start and duration in 12-hour format
    """
    nums = start.split(':')
    temp = nums.pop(1)
    temp = temp.split(' ')
    nums.extend(temp)
    print(nums)

    
    new_time=''
    return new_time

add_time("6:30 PM", "2:00")